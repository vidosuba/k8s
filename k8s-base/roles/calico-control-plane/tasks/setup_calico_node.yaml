---
- name: Update calico-node certificate
  ansible.builtin.include_tasks: get_cert_in_k8s.yaml
  vars:
    vault_role_id: "{{ vault_node_role_id }}"
    vault_secret_id: "{{ vault_node_secret_id }}"
    get_cert_k8s_title: calico-node
    get_cert_k8s_vault_path: "{{ vault_path_prefix }}/{{ vault_cluster_name }}/calico-pki/issue/node"
    get_cert_k8s_vault_data:
      common_name: calico-node
      ttl: 8784h
    get_cert_k8s_secret_name: calico-node-certs
    get_cert_k8s_namespace: kube-system
    get_cert_k8s_filename: calico-node

- name: Apply calico/node resources
  become: true
  environment:
    KUBECONFIG: /etc/kubernetes/admin.conf
  block:
  - name: Create ServiceAccount for calico/node, configure RBAC, ClusterRole, ClusterRoleBinding
    kubernetes.core.k8s:
      definition: "{{ lookup('file', item) }}"
      apply: true
      state: "present"
      validate:
        fail_on_error: true
        strict: true
    with_items:
    - "{{ calico_versions_folder }}/calico-node-serviceaccount.yaml"
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  # Install calico/node Daemon-Set
  # Felix: - Calico per-node daemon
  # BIRD: - Distribution of routing information via the BGP protocol
  # confd: - watches Calico datastore for config changes and updates BIRDs config files
  - name: Deploy Calico node DaemonSet
    kubernetes.core.k8s:
      apply: true
      definition: "{{ lookup('template', item) }}"
      validate:
        fail_on_error: true
        strict: true
      wait: true
    with_items:
    - "{{ calico_versions_folder }}/calico-node-daemonset.yaml.j2"
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

- name: Restart calico/node Pods on certificate renewal  # noqa no-handler
  become: true
  environment:
    KUBECONFIG: /etc/kubernetes/admin.conf
  when: get_cert is changed
  block:
  - name: Restart calico/node Pods  # noqa no-changed-when
    ansible.builtin.command:
      argv:
      - kubectl
      - rollout
      - restart
      - ds
      - calico-node
      - -n
      - kube-system

  - name: Wait for Rollout Restart of calico/node Pods to finish  # noqa no-changed-when
    ansible.builtin.command:
      argv:
      - kubectl
      - rollout
      - status
      - ds
      - calico-node
      - -n
      - kube-system
...
