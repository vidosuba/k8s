# https://github.com/rook/rook/releases/tag/v1.3.11
# $YOUR_ROOK_REPO/cluster/examples/kubernetes/ceph/

{% from "roles/rook_v1/templates/utils.j2" import resource_constraints %}
#################################################################################################################
# Define the settings for the rook-ceph cluster with common settings for a production cluster.
# All nodes with available raw devices will be used for the Ceph cluster. At least three nodes are required
# in this example. See the documentation for more details on storage settings available.

# For example, to create the cluster:
#   kubectl create -f common.yaml
#   kubectl create -f operator.yaml
#   kubectl create -f cluster.yaml
#################################################################################################################

apiVersion: "ceph.rook.io/v1"
kind: "CephCluster"
metadata:
  name: "{{ rook_cluster_name }}"
  namespace: "{{ rook_namespace }}"
spec:
  cephVersion:
    # The container image used to launch the Ceph daemon pods (mon, mgr, osd, mds, rgw).
    # v13 is mimic, v14 is nautilus, and v15 is octopus.
    # RECOMMENDATION: In production, use a specific version tag instead of the general v14 flag, which pulls the latest release and could result in different
    # versions running within the cluster. See tags available at https://hub.docker.com/r/ceph/ceph/tags/.
    # If you want to be more precise, you can always use a timestamp tag such ceph/ceph:v14.2.5-20190917
    # This tag might not contain a new Ceph version, just security fixes from the underlying operating system, which will reduce vulnerabilities
    image: "ceph/ceph:{{ rook_ceph_version }}"
    # Whether to allow unsupported versions of Ceph. Currently `nautilus` and `octopus` are supported.
    # Future versions such as `pacific` would require this to be set to `true`.
    # Do not set to true in production.
    allowUnsupported: false
  # The path on the host where configuration files will be persisted. Must be specified.
  # Important: if you reinstall the cluster, make sure you delete this directory from each host or else the mons will fail to start on the new cluster.
  # In Minikube, the '/data' directory is configured to persist across reboots. Use "/data/rook" in Minikube environment.
  dataDirHostPath: /var/lib/rook
  # Whether or not upgrade should continue even if a check fails
  # This means Ceph's status could be degraded and we don't recommend upgrading but you might decide otherwise
  # Use at your OWN risk
  # To understand Rook's upgrade process of Ceph, read https://rook.io/docs/rook/master/ceph-upgrade.html#ceph-version-upgrades
  skipUpgradeChecks: {{ rook_skip_upgrade_checks | to_json }}
  # Whether or not continue if PGs are not clean during an upgrade
  continueUpgradeAfterChecksEvenIfNotHealthy: false
  # set the amount of mons to be started
  mon:
    count: {{ rook_nmons }}
    allowMultiplePerNode: {{ rook_mon_allow_multiple_per_node }}
{% if rook_mon_volume %}
    # A volume claim template can be specified in which case new monitors (and
    # monitors created during fail over) will construct a PVC based on the
    # template for the monitor's primary storage. Changes to the template do not
    # affect existing monitors. Log data is stored on the HostPath under
    # dataDirHostPath. If no storage requirement is specified, a default storage
    # size appropriate for monitor data will be used.
    volumeClaimTemplate:
      spec:
        storageClassName: "{{ rook_mon_volume_storage_class }}"
        resources:
          requests:
            storage: "{{ rook_mon_volume_size }}"
{% endif %}
  mgr:
{% if rook_mgr_use_pg_autoscaler %}
    modules:
    # Several modules should not need to be included in this list. The "dashboard" and "monitoring" modules
    # are already enabled by other settings in the cluster CR and the "rook" module is always enabled.
    - name: pg_autoscaler
      enabled: true
{% endif %}
  # enable the ceph dashboard for viewing cluster status
  dashboard:
    enabled: false
    # serve the dashboard under a subpath (useful when you are accessing the dashboard via a reverse proxy)
    # urlPrefix: /ceph-dashboard
    # serve the dashboard at the given port.
    # port: 8443
    # serve the dashboard using SSL
    ssl: true
  # enable prometheus alerting for cluster
  monitoring:
    # requires Prometheus to be pre-installed
    enabled: {{ k8s_monitoring_enabled | bool }}
    # namespace to deploy prometheusRule in. If empty, namespace of the cluster will be used.
    # Recommended:
    # If you have a single rook-ceph cluster, set the rulesNamespace to the same namespace as the cluster or keep it empty.
    # If you have multiple rook-ceph clusters in the same k8s cluster, choose the same namespace (ideally, namespace with prometheus
    # deployed) to set rulesNamespace for all the clusters. Otherwise, you will get duplicate alerts with multiple alert definitions.
    rulesNamespace: "{{ rook_namespace }}"
  storage:
    storageClassDeviceSets:
    - name: cinder
      count: {{ rook_nosds }}
      portable: true
{% if rook_osd_anti_affinity or rook_scheduling_key %}
      # Since the OSDs could end up on any node, an effort needs to be made to spread the OSDs
      # across nodes as much as possible. Unfortunately the pod anti-affinity breaks down
      # as soon as you have more than one OSD per node. If you have more OSDs than nodes, K8s may
      # choose to schedule many of them on the same node. What we need is the Pod Topology
      # Spread Constraints.
      # Another approach for a small number of OSDs is to create a separate device set for each
      # zone (or other set of nodes with a common label) so that the OSDs will end up on different
      # nodes. This would require adding nodeAffinity to the placement here.
      placement:
{% if rook_osd_anti_affinity %}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - rook-ceph-osd
                - key: app
                  operator: In
                  values:
                  - rook-ceph-osd-prepare
              topologyKey: kubernetes.io/hostname
        topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: DoNotSchedule
          labelSelector:
            matchExpressions:
            - key: app
              operator: In
              values:
              - rook-ceph-osd
              - rook-ceph-osd-prepare
{% endif %}
{% if rook_scheduling_key %}
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: {{ rook_scheduling_key | to_json }}
                operator: Exists
        tolerations:
        - key: {{ rook_scheduling_key | to_json }}
          operator: Exists
{% endif %}
{% endif %}
      resources: {
{% call resource_constraints(
  rook_osd_memory_request,
  rook_osd_cpu_request,
  rook_osd_memory_limit,
  rook_osd_cpu_limit) %}{% endcall %}
      }
      volumeClaimTemplates:
      - metadata:
          creationTimestamp: null
          name: data  # it is important that the template is called data for rook v1.3
        spec:
          resources:
            requests:
              storage: "{{ rook_osd_volume_size }}"
          storageClassName: "{{ rook_osd_storage_class }}"
          volumeMode: Block
          accessModes:
          - ReadWriteOnce
  removeOSDsIfOutAndSafeToRemove: {{ rook_osd_autodestroy_safe }}
  placement:
{% if rook_scheduling_key %}
    all:
      nodeAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: {{ rook_scheduling_key | to_json }}
              operator: Exists
      tolerations:
      - key: {{ rook_scheduling_key | to_json }}
        operator: Exists
{% endif %}
{% if rook_mon_scheduling_key %}
    mon:
      nodeAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: {{ rook_mon_scheduling_key | to_json }}
              operator: Exists
      tolerations:
      - key: {{ rook_mon_scheduling_key | to_json }}
        operator: Exists
{% endif %}
{% if rook_mgr_scheduling_key %}
    mgr:
      nodeAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: {{ rook_mgr_scheduling_key | to_json }}
              operator: Exists
      tolerations:
      - key: {{ rook_mgr_scheduling_key | to_json }}
        operator: Exists
{% endif %}
  resources:
    mon: {
{% call resource_constraints(
  rook_mon_memory_request,
  rook_mon_cpu_request,
  rook_mon_memory_limit,
  rook_mon_cpu_limit) %}{% endcall %}
    }
    mgr: {
{% call resource_constraints(
  rook_mgr_memory_request,
  rook_mgr_cpu_request,
  rook_mgr_memory_limit,
  rook_mgr_cpu_limit) %}{% endcall %}
    }
    osd: {
{% call resource_constraints(
  rook_osd_memory_request,
  rook_osd_cpu_request,
  rook_osd_memory_limit,
  rook_osd_cpu_limit) %}{% endcall %}
    }
