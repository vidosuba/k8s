# https://github.com/rook/rook/releases/tag/v1.3.11
# $YOUR_ROOK_REPO/cluster/examples/kubernetes/ceph/

{% from "roles/rook_v1/templates/utils.j2" import resource_constraints %}

#################################################################################################################
# Create a filesystem with settings with replication enabled for a production environment.
# A minimum of 3 OSDs on different nodes are required in this example.
#  kubectl create -f filesystem.yaml
#################################################################################################################

apiVersion: ceph.rook.io/v1
kind: CephFilesystem
metadata:
  name: "{{ rook_ceph_fs_name }}"
  namespace: "{{ rook_namespace }}"
spec:
  # The metadata pool spec. Must use replication.
  metadataPool:
    failureDomain: "host"
    replicated:
      size: {{ rook_ceph_fs_replicated }}
      requireSafeReplicaSize: {{ (rook_ceph_fs_replicated | int != 1) | to_json }}
    compressionMode: "none"
    parameters:
      # Inline compression mode for the data pool
      # Further reference: https://docs.ceph.com/docs/nautilus/rados/configuration/bluestore-config-ref/#inline-compression
      compressionMode: "none"
        # gives a hint (%) to Ceph in terms of expected consumption of the total cluster capacity of a given pool
      # for more info: https://docs.ceph.com/docs/master/rados/operations/placement-groups/#specifying-expected-pool-size
      #target_size_ratio: ".5"
  # The list of data pool specs. Can use replication or erasure coding.
  dataPools:
    - failureDomain: host
      replicated:
        size: {{ rook_ceph_fs_replicated }}
        # Disallow setting pool with replica 1, this could lead to data loss without recovery.
        # Make sure you're *ABSOLUTELY CERTAIN* that is what you want
        requireSafeReplicaSize: {{ (rook_ceph_fs_replicated | int != 1) | to_json }}
      compressionMode: "none"
      parameters:
        # Inline compression mode for the data pool
        # Further reference: https://docs.ceph.com/docs/nautilus/rados/configuration/bluestore-config-ref/#inline-compression
        compressionMode: "none"
          # gives a hint (%) to Ceph in terms of expected consumption of the total cluster capacity of a given pool
        # for more info: https://docs.ceph.com/docs/master/rados/operations/placement-groups/#specifying-expected-pool-size
        #target_size_ratio: ".5"
  # Whether to preserve metadata and data pools on filesystem deletion
  preservePoolsOnDelete: true
  # The metadata service (mds) configuration
  metadataServer:
    # The number of active MDS instances
    activeCount: 1
    # Whether each active MDS instance will have an active standby with a warm metadata cache for faster failover.
    # If false, standbys will be available, but will not have a warm cache.
    activeStandby: true
    # A key/value list of annotations
    annotations:
    #  key: value
    # The affinity rules to apply to the mds deployment
    placement:
{% if rook_scheduling_key %}
      nodeAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: {{ rook_scheduling_key | to_json }}
              operator: Exists
      tolerations:
      - key: {{ rook_scheduling_key | to_json }}
        operator: Exists
{% endif %}
#       podAntiAffinity:
#          requiredDuringSchedulingIgnoredDuringExecution:
#          - labelSelector:
#              matchExpressions:
#              - key: app
#                operator: In
#                values:
#                - rook-ceph-mds
#            # topologyKey: kubernetes.io/hostname will place MDS across different hosts
#            topologyKey: kubernetes.io/hostname
#          preferredDuringSchedulingIgnoredDuringExecution:
#          - weight: 100
#            podAffinityTerm:
#              labelSelector:
#                matchExpressions:
#                - key: app
#                  operator: In
#                  values:
#                  - rook-ceph-mds
#              # topologyKey: */zone can be used to spread MDS across different AZ
#              # Use <topologyKey: failure-domain.beta.kubernetes.io/zone> in k8s cluster if your cluster is v1.16 or lower
#              # Use <topologyKey: topology.kubernetes.io/zone>  in k8s cluster is v1.17 or upper
#              topologyKey: topology.kubernetes.io/zone
    #resources:
    # The requests and limits set here, allow the filesystem MDS Pod(s) to use half of one CPU core and 1 gigabyte of memory
    #  limits:
    #    cpu: "500m"
    #    memory: "1024Mi"
    #  requests:
    #    cpu: "500m"
    #    memory: "1024Mi"
    priorityClassName: system-cluster-critical
    resources: {
{% call resource_constraints(
  rook_mds_memory_request,
  rook_mds_cpu_request,
  rook_mds_memory_limit,
  rook_mds_cpu_limit) %}{% endcall %}
    }
