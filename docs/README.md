# Managed Kubernetes Handbook

This docu is generated using sphinx.

## Table of Contents

See [index.rst](index.rst).

## How to render

Install sphinx by executing

```
pip3 install sphinx sphinx-rtd-theme sphinx-tabs myst-parser furo
```

To build the documentation use

```
python3 -m sphinx docs _build/html
```
