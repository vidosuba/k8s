Load Balancing
==============

.. toctree::
   :maxdepth: 2
   :caption: Load Balancing
   :hidden:

   ch-k8s-lbaas

On OpenStack the following self-developed load-balancing solution is proposed: :doc:`ch-k8s-lbaas </managed-services/load-balancing/ch-k8s-lbaas>`.
