Prometheus-based Monitoring
===========================

.. toctree::
   :maxdepth: 2
   :caption: Monitoring
   :hidden:

   prometheus-stack
   global-monitoring


This LCM uses the
`helm-based monitoring stack <https://github.com/prometheus-community/helm-charts>`__
provided by the Prometheus community.

.. todo::

   needs details
